;;; dropboxer.el --- Helps finding and accessing a local dropbox folder -*- lexical-binding: t -*-

;; Copyright (C) 2018 Norbert Melzer

;; Author: Norbert Melzer <timmelzer@gmail.com>
;; Created: 14 Jan 2018
;; Keywords: comm
;; Homepage: https://gitlab.com/NobbZ/emacs-dropboxer
;; Package-Requires: (f json)
;; Version: 0.1.1

;; This file is not part of GNU Emacs.

;;; Commentary:

;;; Code:

(require 'f)
(require 'json)

(defconst dropboxer-version "0.1.1"
  "Dropboxer version.")

(defvar dropboxer-base-path nil
  "Userspecified root-path for a dropbox.")

;;;###autoload
(defun dropboxer-root ()
  "Return the root-path of the dropbox."
  (interactive)
  (or dropboxer-base-path
      (dropboxer--root #'json-read-file)))

;;;###autoload
(defun dropboxer-expand-file-name (name)
  "Return the full path of the file given as `NAME'."
  (interactive)
  (dropboxer--expand #'json-read-file name))

;;;###autoload
(defun dropboxer-join (&rest args)
  "Joins the dropbox path and the `ARGS' to a proper path."
  (dropboxer-expand-file-name (apply #'f-join args)))

(defun dropboxer--root (reader)
  (dropboxer--update-and-get-base-path reader))

(defun dropboxer--expand (reader name)
  (expand-file-name (f-join (dropboxer--root reader) name)))

(defun dropboxer--join (reader &rest args)
  (dropboxer--expand reader (apply #'f-join args)))

(defun dropboxer--update-and-get-base-path (reader)
  (setq dropboxer-base-path (dropboxer--read-path-from-config
                             reader
                             (dropboxer--find-dropbox-config)))
  dropboxer-base-path)

(defun dropboxer--read-path-from-config (reader config)
  (cdr (assoc 'path (car (funcall reader config)))))

(defun dropboxer--find-dropbox-config ()
  (expand-file-name
   (if (eq system-type 'gnu/linux)
       (f-join (getenv "HOME") ".dropbox" "info.json")
     (f-join (getenv "LOCALAPPDATA") "Dropbox" "info.json"))))

(provide 'dropboxer)

;;; dropboxer.el ends here
