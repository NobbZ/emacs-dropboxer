;;; emacs-dropboxer-test.el --- Tests for emacs-dropboxer

(require 'ert)

(require 'dropboxer)

(setq dropboxer-config-stub "{\"personal\": {\
\"path\": \"/home/stubber/Dropbox\",\
\"host\": 0,\
\"is_team\": false,\
\"subscription_type\": \"Basic\"}}")

(defun dropboxer--reader-stub (_)
  (json-read-from-string dropboxer-config-stub))

(ert-deftest find-root ()
  (should (string= (dropboxer--root #'dropboxer--reader-stub)
                   "/home/stubber/Dropbox")))

(ert-deftest find-file-in-dropbox ()
  (should (string= (dropboxer--expand #'dropboxer--reader-stub "foo.txt")
                   "/home/stubber/Dropbox/foo.txt")))

(ert-deftest find-file-in-dropbox-join ()
  (should (string= (dropboxer--join #'dropboxer--reader-stub "foo" "bar.txt")
                   "/home/stubber/Dropbox/foo/bar.txt")))

;;; emacs-dropboxer-test.el ends here
