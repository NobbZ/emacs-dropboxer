;;; test-helper.el --- Helpers for emacs-dropboxer-test.el

(defvar dropboxer-test/test-path
  (f-dirname load-file-name))

(defvar dropboxer-test/root-path
  (f-parent dropboxer-test/test-path))

(add-to-list 'load-path dropboxer-test/root-path)

;;; test-helper.el ends here
