SRC_FILES = $(shell find . -type f                \
                           -name '*.el'           \
                           -not -path './.cask/*' \
                           -not -path './test/*')
TST_FILES = $(shell find test/ -type f -name '*-test.el')
ELC_FILES = ${SRC_FILES:%.el=%.elc}

all: .cask-install ${ELC_FILES}

clean: ${ELC_FILES:%=%-rm-f} .cask-rm-rf .cask-install-rm-f

test: .cask-install ${ELC_FILES}
	cask exec ert-runner
.PHONY: test

# build helpers

.cask-install: Cask
	cask install
	touch $@

%.elc: %.el
	cask eval '(byte-compile-file "$<")'

# clean helpers

%-rm-f:
	rm -f $*

%-rm-rf:
	rm -rf $*

